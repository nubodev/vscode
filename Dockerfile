FROM alpine
ADD vscode.tar.gz /opt
VOLUME /opt/vscode
CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
